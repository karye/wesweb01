# Felmeddelanden

Några extra rader i börja på sidan för att aktivera felmeddelanden:

```php
<?php
// Aktivera felmeddelanden under utveckling
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
```

När websidan är klar kommenteras raderna bort:

```php
<?php
// Aktivera felmeddelanden under utveckling
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);
```
