# Hosting

## Azure i VS Code

### Registrera på Azure Portal

* [https://azure.microsoft.com/en-us/free/students/](https://azure.microsoft.com/en-us/free/students/)

### VS Code

* Installera Azure tillägg för VS Code:
  * [https://marketplace.visualstudio.com/items?itemName=ms-vscode.azure-account](https://marketplace.visualstudio.com/items?itemName=ms-vscode.azure-account)
  * [https://marketplace.visualstudio.com/items?itemName=ms-azuretools.vscode-azureappservice](https://marketplace.visualstudio.com/items?itemName=ms-azuretools.vscode-azureappservice)
  * [https://marketplace.visualstudio.com/items?itemName=ms-azuretools.vscode-azureresourcegroups](https://marketplace.visualstudio.com/items?itemName=ms-azuretools.vscode-azureresourcegroups)

![Registrera på Azure](<../.gitbook/assets/image (109) (1) (1).png>)

### Skapa en PHP-webbapp i Azure App Service

* Följ instruktionerna på [https://docs.microsoft.com/sv-se/azure/app-service/quickstart-php](https://docs.microsoft.com/sv-se/azure/app-service/quickstart-php)

