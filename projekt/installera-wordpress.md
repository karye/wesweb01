---
description: Hur man installerar Wordpress på en LAMP-server
---

# Installera Wordpress

## Översikt

![](<../.gitbook/assets/image (88).png>)

WordPress är det mest populära open-source bloggsystemet och CMS på webben. Den är baserad på PHP och MySQL. Dess funktioner kan utökas med tusentals gratis plugins och teman.

I denna guide kommer vi att installera WordPress på Apache2-servern och skapa vårt första inlägg.

### Vad du lär dig

* Hur du installerar WordPress
* Hur du konfigurerar WordPress
* Hur du skapar det första inlägget

### Vad du behöver

* En dator som kör Ubuntu Server LTS
* Den här guiden visar dig också hur du konfigurerar en databas för WordPress

## Terminalen

1. Gå till **Docker** tillägget i VS Code
2. Högerklicka på din **LAMP-kontainer** (karye/lampw-latest)
3. Välj **Attach Shell**

![](<../.gitbook/assets/image (91).png>)

Terminalen är startad:

![](<../.gitbook/assets/image (90).png>)

## Installera paketberoenden

{% hint style="warning" %}
Var **mycket noga** med kommandon\
Läs noga och kopiera precis det som står\
Tex kopiera "apt update"...\
och klistra in "apt update" i din terminal
{% endhint %}

För att installera PHP och Apache, kör följande kommandon:

```bash
apt update
```

![Så kan de se ut när du kör "apt update"](<../.gitbook/assets/image (92).png>)

Hämta senaste uppdateringar:

```bash
apt upgrade
```

Installera alla paket som Wordpress kräver:

```bash
apt install php-bcmath php-curl php-imagick php-intl php-json php-mbstring php-mysql php-xml php-zip
```

## Installera WordPress

Vi kommer att använda versionen från [WordPress.org](https://wordpress.org) snarare än APT-paketet i Ubuntu -arkivet, eftersom detta är den föredragna metoden från uppströms WordPress. Detta kommer också att ha färre "gotcha" -problem som WordPress -volontärerna inte kommer att kunna förutse och därför inte kan hjälpa till med.

Skapa installationskatalogen:

```bash
mkdir -p /srv/www
```

Och ladda ner filen från [WordPress.org](https://wordpress.org):

```bash
curl https://wordpress.org/latest.tar.gz | tar zx -C /srv/www
```

Gör mappen läsbar för Apache2:

```bash
chown -R www-data: /srv/www
```

## Konfigurera installationen

### Konfigurerar Apache för WordPress

Skapa en **Apache** site för **WordPress**. Skapa`/etc/apache2/sites-available/wordpress.conf` :

```bash
nano /etc/apache2/sites-available/wordpress.conf
```

Klistra in följande rader:

```
DocumentRoot /srv/www/wordpress
Alias /wordpress /srv/www/wordpress
<Directory /srv/www/wordpress>
    Options FollowSymLinks
    AllowOverride Limit Options FileInfo
    DirectoryIndex index.php
    Require all granted
</Directory>
<Directory /srv/www/wordpress/wp-content>
    Options FollowSymLinks
    Require all granted
</Directory>
```

![](<../.gitbook/assets/image (94).png>)

Spara och stäng konfigurationsfilen genom att skriva **Ctrl+X** följt av **Y** och sedan **Enter**.

Aktivera sedan den här webbplatsen med:

```bash
a2ensite wordpress
```

Aktivera URL-rewriting med:

```bash
a2enmod rewrite
```

Starta om apache2:

```bash
service apache2 reload
```

### Konfigurera databasen

För att konfigurera WordPress måste vi skapa en MySQL-databas. Vi gör det!

Logga in i databasmotorn:

```
mysql -u root
```

![Så ser det ut när man loggat in](<../.gitbook/assets/image (93).png>)

Skapa nu en databas:

```
CREATE DATABASE wordpress;
```

Skapa en databasanvändare:

{% hint style="warning" %}
_**Ersätt**_ `your-password`med ett databaslösenord som du väljer
{% endhint %}

```
CREATE USER wordpress@localhost IDENTIFIED BY 'your-password';
```

Ställ in rättigheter:

```
GRANT SELECT,INSERT,UPDATE,DELETE,CREATE,DROP,ALTER ON wordpress.* TO wordpress@localhost;
```

Rensa:

```
FLUSH PRIVILEGES;
```

Logga ur ut databasmotorn:

```
quit;
```

Starta om **MySQL** med:

```bash
service mysql start
```

### Konfigurera WordPress för att ansluta till databasen

Låt oss nu konfigurera WordPress för att använda denna databas. Kopiera först konfigurationsfilen till `wp-config.php`:

```bash
cp -a /srv/www/wordpress/wp-config-sample.php /srv/www/wordpress/wp-config.php
```

Ange sedan databasuppgifterna i konfigurationsfilen:

```bash
sed -i 's/database_name_here/wordpress/' /srv/www/wordpress/wp-config.php
```

```bash
sed -i 's/username_here/wordpress/' /srv/www/wordpress/wp-config.php
```

{% hint style="warning" %}
_**Ersätt**_ `your-password`med ditt databaslösenord
{% endhint %}

```bash
sed -i 's/password_here/your-password/' /srv/www/wordpress/wp-config.php
```

Slutligen öppnar du konfigurationsfilen i nano:

```bash
nano /srv/www/wordpress/wp-config.php
```

![](<../.gitbook/assets/image (95).png>)

Hitta följande rader:

```php
define( 'AUTH_KEY',         'put your unique phrase here' );
define( 'SECURE_AUTH_KEY',  'put your unique phrase here' );
define( 'LOGGED_IN_KEY',    'put your unique phrase here' );
define( 'NONCE_KEY',        'put your unique phrase here' );
define( 'AUTH_SALT',        'put your unique phrase here' );
define( 'SECURE_AUTH_SALT', 'put your unique phrase here' );
define( 'LOGGED_IN_SALT',   'put your unique phrase here' );
define( 'NONCE_SALT',       'put your unique phrase here' );
```

Ta bort dessa rader. Ersätt sedan med innehållet på [https://api.wordpress.org/secret-key/1.1/salt](https://api.wordpress.org/secret-key/1.1/salt). (Den här adressen är en randomiserare som returnerar helt slumpmässiga nycklar varje gång den öppnas.) Det här steget är viktigt för att säkerställa att din webbplats inte är sårbar för "kända hemligheter" -attacker.

Spara och stäng konfigurationsfilen genom att skriva **Ctrl+X** följt av **Y** och sedan **Enter**.

## Konfigurera WordPress

Öppna [http://localhost:8080/wordpress](http://localhost:8080/wordpress) i din webbläsare. Du kommer att bli ombedd att namnge din nya webbplats, användarnamn, lösenord och e-postadress. Observera att det användarnamn och lösenord du väljer här är för WordPress, och inte ger åtkomst till någon annan del av din server - välj ett användarnamn och lösenord som skiljer sig från dina MySQL (databas) referenser, som vi konfigurerade för WordPress användning, och skiljer sig från dina referenser för att logga in på din dator eller serverns skrivbord eller skal. Du kan välja om du vill göra din webbplats indexerad av sökmotorer.

![](<../.gitbook/assets/image (82).png>)

![](<../.gitbook/assets/image (83).png>)

Du kan nu logga in under [http://localhost:8080/wordpress/wp-login.php](http://localhost:8080/wordpress/wp-login.php). I WordPress Dashboard ser du en massa ikoner och alternativ. Oroa dig inte, det är enkelt!

![](<../.gitbook/assets/image (84).png>)

### Skapa ditt första inlägg

Du ser inlägget "Hello World!". Låt oss ta bort det och skriva något mer intressant:

![](<../.gitbook/assets/image (85).png>)

På instrumentpanelen väljer du ikonen "Inlägg" och klickar på "Alla inlägg". Håll musen över "Hello World!" posttitel och välj papperskorgen.

För att skapa ett nytt inlägg, klicka på knappen "Lägg till nytt". Du bör märka en snygg WYSIWYG -editor med enkla (men kraftfulla) textformateringsalternativ. Du kanske vill byta till textläge om du föredrar ren HTML.

Låt oss skriva något! Det är lika enkelt som att använda textprocessorer som du känner till från kontorssviter.

![](<../.gitbook/assets/image (86).png>)

Klicka nu på knappen Publicera. Nu kan du se ditt helt nya inlägg!

### Det är allt!

Naturligtvis har denna handledning bara beskrivit grunderna i WordPress -användning, du kan göra mycket mer med denna bloggplattform/CMS. Du kan installera en av tusentals tillgängliga (gratis och kommersiella) plugins och teman. Du kan till och med konfigurera det som forum (med bbPress -plugin), mikrobloggplattform (BuddyPress), e -handelsplattform (WooCommerce) eller utöka befintliga WordPress -funktioner med plugins som JetPack eller TinyMCE Advanced.

WordPress -manualen och dokumentationen finns på WordPress -dokumentationssidorna. Du kan läsa den för att lära dig mer om WordPress -användning och till och med något om teman/plugins -utveckling.

Om du behöver mer vägledning om hur du använder WordPress finns hjälp alltid till hands:

* [WordPress.org Forums](https://wordpress.org/support/forums/)
* [Ask Ubuntu](https://askubuntu.com)
* [Ubuntu Forums](https://ubuntuforums.org)

### Läs mera

* [WordPress Documentation](https://wordpress.org/support/)
* [WordPress page in Ubuntu documentation](https://help.ubuntu.com/lts/serverguide/wordpress.html)

_Ursprungligen författad av Marcin Mikołajczak Tungt uppdaterad av Dani Llewellyn_
