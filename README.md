---
description: Introduktion till kursen webbserverprogrammering 1
---

# Introduktion

## Centralt innehåll

Undervisningen i kursen ska behandla följande centrala innehåll:

* **Webbserverns och dynamiska webbplatsers funktionalitet.**
* **Utvecklingsprocessen för ett webbtekniskt projekt med målsättningar, krav, begränsningar, planering och uppföljning. Specifikation av struktur och design, kodning, testning samt driftsättning.**
* Dokumentation av utvecklingsprocess och färdig produkt.
* En översikt över olika lösningar eller språk som finns för att skapa dynamiska webbplatser.
* **Grundfunktionen i ett programspråk för dynamiska webbplatser.**
* **Teknisk orientering om webbens protokoll, adresser, säkerhet samt samspelet mellan klient och server.**
* **Datalagring i relationsdatabas eller med annan teknik.**
* Teckenkodning. Begrepp, standarder och handhavande.
* **Kodning och dokumentation enligt vedertagen praxis för vald teknik.**
* **Applikationsarkitektur och separation av olika slags logik.**
* Kvalitetssäkring av dynamiska webbapplikationers funktionalitet, säkerhet och kodkvalitet.
* **Grundläggande säkerhet och sätt att identifiera hot och sårbarheter samt hur attacker kan motverkas genom effektiva åtgärder.**

## **Konkretisering**

* [ ] Förstå hur en webbserver får en dynamisk webbsida att funka
* [ ] Kunna bygga dynamiska webbsidor med PHP
* [ ] Kunna bygga en dynamisk webbplats med meny
* [ ] Kunna bygga en webbplats med formulär
* [ ] Kunna lagra data
  * [ ] Läsa/skriva i filer
  * [ ] Använda en databas
* [ ] Kunna säkra webbplatsen
  * [ ] Skydda med lösenord
  * [ ] Skydda med inloggning

## Bra webblänkar

* [https://devdocs.io](https://devdocs.io)
* [http://orion.lnu.se/pub/education/course/1IK424/VT13/sessions/F01.html](http://orion.lnu.se/pub/education/course/1IK424/VT13/sessions/F01.html)
* [http://orion.lnu.se/pub/education/course/1IK424/VT13/sessions/F02.html](http://orion.lnu.se/pub/education/course/1IK424/VT13/sessions/F02.html#1)
* [http://orion.lnu.se/pub/education/course/1IK424/VT13/sessions/F03.html](http://orion.lnu.se/pub/education/course/1IK424/VT13/sessions/F03.html)

## Material

{% hint style="info" %}
Från PHP-kompendiet av Thomas Höjemo, © SNT 2006, www.snt.se\
[http://www.snt.se/phpkompendium.pdf](http://www.snt.se/phpkompendium.pdf)\
[http://www.snt.se/phpfacit.pdf](http://www.snt.se/phpfacit.pdf)
{% endhint %}

{% hint style="info" %}
Copyright © 2004, 2005 Rejås Datakonsult. Var och en äger rätt att kopiera, sprida och/eller förändra detta dokument under villkoren i licensen "GNU Free Documentation License", version 1.2 eller senare publicerad av Free Software Foundation, utan oföränderliga avsnitt, utan framsidestexter och utan baksidestexter. En kopia av denna licens finns på [http://rejas.se/gnu/.](http://rejas.se/gnu/)
{% endhint %}
