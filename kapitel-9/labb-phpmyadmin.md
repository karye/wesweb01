# Labb - phpMyAdmin

## Skapa en databas

### Skapa användare

I phpmyadmin går vi till fliken **Användarkonton** och klickar på **Lägg till Användare**:

![](../.gitbook/assets/phpmyadmin-1.png)

### Skapa databas

Vi fyller i uppgifterna, kryssar i **Skapa databas med samma namn...** och klickar **Kör:**

![](../.gitbook/assets/phpmyadmin-2.png)

### Logga in på mysql

Nu kan vi logga in på databasen och med användaren som vi skapat:

![](../.gitbook/assets/phpmyadmin-3.png)

## Importera data

### datafil i csv-format

Vi har en textfil som innehåller data om restauranger:

{% file src="../.gitbook/assets/restauranger.csv" %}

![](<../.gitbook/assets/phpmyadmin-4 (1).png>)

### Importera i phpMyAdmin

I phpMyadmin gör vi följande:

* Klickar på databasen **movies\_db** vi skapat
* Väljer fliken **Importera**
* Väljer filen **restauranger.csv**
* Kryssar för **Den första raden innehåller kolumnnamn...**
* Klickar på **Kör**

![](../.gitbook/assets/phpmyadmin-6.png)

Nu fick vi nya rader i en tabell som heter **TABLE 1**:

![](../.gitbook/assets/phpmyadmin-7.png)

### Städa upp

Vi ändrar namn på tabellen i fliken **Operationer:**

![](../.gitbook/assets/phpmyadmin-8.png)

### Rätta till tabellens struktur

I fliken **Struktur** ser vi kolumnerna i tabellen:

* Markera **alla** 4 kolumner
* Klicka på **Ändra**

![](../.gitbook/assets/phpmyadmin-9.png)

Nu kan vi redigera kolumnernas namn och datatyp till följande:

* Ändra kolumn namn till **varchar(50)**
* Ändra gatuadress till **varchar(50)**
* Ändra postort namn till **varchar(20)**

![](../.gitbook/assets/phpmyadmin-10.png)

### Tabellen skall ha kolumn id

Alla tabeller skall ha en **kolumn id:**

* Nedanför raderna med kolumnerna väljer vi **I början av tabellen**
* Klicka på **Kör**

![](../.gitbook/assets/phpmyadmin-12.png)

* Döp nya kolumnen till **id**:

![](../.gitbook/assets/phpmyadmin-14.png)

* Kryssa i **A\_I** och klicka **Kör**:
* Avsluta med **Spara**

![](../.gitbook/assets/phpmyadmin-15.png)

Nu är tabellen klar:

![](../.gitbook/assets/phpmyadmin-13.png)

## Fritextsökning med sql

```sql
MariaDB [movies_db]> SHOW TABLES;
Empty set (0.00 sec)

MariaDB [movies_db]> SELECT * FROM movies;
+----+-------------------------------+-------------------------+--------+-----------+
| id | namn                          | gatuadress              | postnr | postort   |
+----+-------------------------------+-------------------------+--------+-----------+
|  1 | Bagel Street Cafe             | Kungsgatan 42           |  11156 | Stockholm |
|  2 | Café Cups                     | Fleminggatan 18         |  11226 | Stockholm |
|  3 | Dagobert restaurang           | Roslagsgatan 7          |  11355 | Stockholm |
|  4 | Falafel Kungen                | Sveavägen 71            |  11350 | Stockholm |
|  5 | G&E Pizzeria / Vasa La grande | Dalagatan 32            |  11324 | Stockholm |
|  6 | Göran Terrassen               | Sankt Göransplan 1      |  11219 | Stockholm |
|  7 | Il Piccio Ristorante          | Fridhemsgatan 3         |  11240 | Stockholm |
|  8 | Kebab Kungen                  | Odengatan 54            |  11322 | Stockholm |
|  9 | Mam Restaurang                | Hagagatan 44            |  11347 | Stockholm |
| 10 | Nybergs Konditori             | Upplandsgatan 26        |  11326 | Stockholm |
| 11 | O´Mamma Mia                   | Kungstensgatan 60       |  11329 | Stockholm |
| 12 | Restaurang Aroti              | Wallingatan 40          |  11124 | Stockholm |
| 13 | Sterix                        | Polhemsgatan 50         |  11230 | Stockholm |
| 14 | Subway Hötorget               | Subway Hötorget T-bana  |  11156 | Stockholm |
| 15 | Subway Karlbergsvägen         | Karlbergsvägen 16       |  11327 | Stockholm |
| 16 | Subway Sankt Eriksplan        | Sankt Eriksplan 17      |  11320 | Stockholm |
| 17 | Subway Sveavägen/Olofsgatan   | Sveavägen 33            |  11134 | Stockholm |
| 18 | Subway                        | Odengatan 49            |  11351 | Stockholm |
| 19 | Sushirullen Odenplan          | Norrtullsgatan 10       |  11327 | Stockholm |
| 20 | Taco Bar Sveavägen            | Sveavägen 108           |  11350 | Stockholm |
| 21 | TacoBar                       | Sankt Eriksplan 5       |  11320 | Stockholm |
| 22 | Valkyria                      | Crafoordsv 12           |  11324 | Stockholm |
+----+-------------------------------+-------------------------+--------+-----------+
22 rows in set (0.00 sec)
```

Låt säg vi söker efter alla Subway-restauranger. Vi försöker med:
```sql
MariaDB [movies_db]> SELECT * FROM movies WHERE namn = "Subway";
+----+--------+--------------+--------+-----------+
| id | namn   | gatuadress   | postnr | postort   |
+----+--------+--------------+--------+-----------+
| 18 | Subway | Odengatan 49 |  11351 | Stockholm |
+----+--------+--------------+--------+-----------+
1 row in set (0.00 sec)
```
Men vi får bara en träff.  
Vi hittar inte alla restauranger för att fältet namn innehåller mer än bara ordet Subway.
Vi provar med **LIKE**:
```sql
MariaDB [movies_db]> SELECT * FROM movies WHERE namn LIKE "Subway";
+----+--------+--------------+--------+-----------+
| id | namn   | gatuadress   | postnr | postort   |
+----+--------+--------------+--------+-----------+
| 18 | Subway | Odengatan 49 |  11351 | Stockholm |
+----+--------+--------------+--------+-----------+
1 row in set (0.01 sec)
```
Vi får ändå bara en träff.  
Det som saknas är ett **jokertecken** för att säga att vi skall matcha även övriga tecken, dvs **Subway%**:
```sql
MariaDB [movies_db]> SELECT * FROM movies WHERE namn LIKE "Subway%";
+----+------------------------------+-------------------------+--------+-----------+
| id | namn                         | gatuadress              | postnr | postort   |
+----+------------------------------+-------------------------+--------+-----------+
| 14 | Subway Hötorget              | Subway Hötorget T-bana  |  11156 | Stockholm |
| 15 | Subway Karlbergsvägen        | Karlbergsvägen 16       |  11327 | Stockholm |
| 16 | Subway Sankt Eriksplan       | Sankt Eriksplan 17      |  11320 | Stockholm |
| 17 | Subway Sveavägen/Olofsgatan  | Sveavägen 33            |  11134 | Stockholm |
| 18 | Subway                       | Odengatan 49            |  11351 | Stockholm |
+----+------------------------------+-------------------------+--------+-----------+
5 rows in set (0.00 sec)
```
Nu hittar vi alla restauranger som börjar med ordet **Subway**.
Om vi vill hitta restauranger som slutar på **vägen** skriver vi **%vagen**:
```sql
MariaDB [movies_db]> SELECT * FROM movies WHERE namn LIKE "%vagen";
+----+------------------------+--------------------+--------+-----------+
| id | namn                   | gatuadress         | postnr | postort   |
+----+------------------------+--------------------+--------+-----------+
| 15 | Subway Karlbergsvägen  | Karlbergsvägen 16  |  11327 | Stockholm |
| 20 | Taco Bar Sveavägen     | Sveavägen 108      |  11350 | Stockholm |
+----+------------------------+--------------------+--------+-----------+
2 rows in set (0.00 sec)
```
På samma sätt kan vi hitta alla restauranger med ordet **vagen** någonstans i namnet med **%vagen%**:
```sql
MariaDB [movies_db]> SELECT * FROM movies WHERE namn LIKE "%vagen%";
+----+------------------------------+--------------------+--------+-----------+
| id | namn                         | gatuadress         | postnr | postort   |
+----+------------------------------+--------------------+--------+-----------+
| 15 | Subway Karlbergsvägen        | Karlbergsvägen 16  |  11327 | Stockholm |
| 17 | Subway Sveavägen/Olofsgatan  | Sveavägen 33       |  11134 | Stockholm |
| 20 | Taco Bar Sveavägen           | Sveavägen 108      |  11350 | Stockholm |
+----+------------------------------+--------------------+--------+-----------+
3 rows in set (0.00 sec)
```

Ett annat jokertecken är **_** som står för ett-tecken-vilket-helst.  
Vi kan använda det för hitta namn som bara börjar med ord 4-tecken-långt:
```sql
MariaDB [movies_db]> SELECT * FROM movies WHERE namn LIKE "____ %";
+----+---------------------+-----------------+--------+-----------+
| id | namn                | gatuadress      | postnr | postort   |
+----+---------------------+-----------------+--------+-----------+
|  2 | Café Cups           | Fleminggatan 18 |  11226 | Stockholm |
| 20 | Taco Bar Sveavägen  | Sveavägen 108   |  11350 | Stockholm |
+----+---------------------+-----------------+--------+-----------+
2 rows in set (0.00 sec)
```
## Uppgifter
### Uppgift 1
Hitta alla restauranger som börjar på **S**:
```sql
+----+------------------------------+-------------------------+--------+-----------+
| id | namn                         | gatuadress              | postnr | postort   |
+----+------------------------------+-------------------------+--------+-----------+
| 13 | Sterix                       | Polhemsgatan 50         |  11230 | Stockholm |
| 14 | Subway Hötorget              | Subway Hötorget T-bana  |  11156 | Stockholm |
| 15 | Subway Karlbergsvägen        | Karlbergsvägen 16       |  11327 | Stockholm |
| 16 | Subway Sankt Eriksplan       | Sankt Eriksplan 17      |  11320 | Stockholm |
| 17 | Subway Sveavägen/Olofsgatan  | Sveavägen 33            |  11134 | Stockholm |
| 18 | Subway                       | Odengatan 49            |  11351 | Stockholm |
| 19 | Sushirullen Odenplan         | Norrtullsgatan 10       |  11327 | Stockholm |
+----+------------------------------+-------------------------+--------+-----------+
7 rows in set (0.00 sec)
```
### Uppgift 2
Hitta alla restauranger där gatuadress innehåller **gatan**:
```sql
+----+-------------------------------+-------------------+--------+-----------+
| id | namn                          | gatuadress        | postnr | postort   |
+----+-------------------------------+-------------------+--------+-----------+
|  1 | Bagel Street Cafe             | Kungsgatan 42     |  11156 | Stockholm |
|  2 | Café Cups                     | Fleminggatan 18   |  11226 | Stockholm |
|  3 | Dagobert restaurang           | Roslagsgatan 7    |  11355 | Stockholm |
|  5 | G&E Pizzeria / Vasa La grande | Dalagatan 32      |  11324 | Stockholm |
|  7 | Il Piccio Ristorante          | Fridhemsgatan 3   |  11240 | Stockholm |
|  8 | Kebab Kungen                  | Odengatan 54      |  11322 | Stockholm |
|  9 | Mam Restaurang                | Hagagatan 44      |  11347 | Stockholm |
| 10 | Nybergs Konditori             | Upplandsgatan 26  |  11326 | Stockholm |
| 11 | O´Mamma Mia                   | Kungstensgatan 60 |  11329 | Stockholm |
| 12 | Restaurang Aroti              | Wallingatan 40    |  11124 | Stockholm |
| 13 | Sterix                        | Polhemsgatan 50   |  11230 | Stockholm |
| 18 | Subway                        | Odengatan 49      |  11351 | Stockholm |
| 19 | Sushirullen Odenplan          | Norrtullsgatan 10 |  11327 | Stockholm |
+----+-------------------------------+-------------------+--------+-----------+
13 rows in set (0.00 sec)
```
### Uppgift 3
Hitta alla restauranger har ordet **la** i namnet:
```sql
+----+-------------------------------+--------------+--------+-----------+
| id | namn                          | gatuadress   | postnr | postort   |
+----+-------------------------------+--------------+--------+-----------+
|  5 | G&E Pizzeria / Vasa La grande | Dalagatan 32 |  11324 | Stockholm |
+----+-------------------------------+--------------+--------+-----------+
1 row in set (0.00 sec)
```
### Uppgift 4
Hitta alla restauranger som innehåller **la**:
```sql
+----+-------------------------------+--------------------+--------+-----------+
| id | namn                          | gatuadress         | postnr | postort   |
+----+-------------------------------+--------------------+--------+-----------+
|  4 | Falafel Kungen                | Sveavägen 71       |  11350 | Stockholm |
|  5 | G&E Pizzeria / Vasa La grande | Dalagatan 32       |  11324 | Stockholm |
| 16 | Subway Sankt Eriksplan        | Sankt Eriksplan 17 |  11320 | Stockholm |
| 19 | Sushirullen Odenplan          | Norrtullsgatan 10  |  11327 | Stockholm |
+----+-------------------------------+--------------------+--------+-----------+
4 rows in set (0.00 sec)

MariaDB [movies_db]> quit;
```

