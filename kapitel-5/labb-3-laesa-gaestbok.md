---
description: Vi hämtar data från backend-servern
---

# Labb 3 - läsa gästbok

## Backend-sida för att läsa

Använd [bootstrap](https://getbootstrap.com/docs/5.1/getting-started/introduction/) för att styla sidan.

{% tabs %}
{% tab title="lasa.php" %}
```php
<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Gästboken</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="kontainer">
        <h1 class="display-4">Gästboken</h1>
        <?php
        // Läs in textfilen
        ...

        // Skriv ut allt
        ...

        ?>
    </div>
</body>
</html>
```
{% endtab %}

{% tab title="style.css" %}
```css
@import url('https://fonts.googleapis.com/css?family=Source+Sans+Pro&display=swap');

/* Enkel CSS-reset */
html {
    box-sizing: border-box;
}
*, *:before, *:after {
    box-sizing: inherit;
}
body, h1, h2, h3, h4, h5, h6, p, ul {
    margin: 0;
    padding: 0;
}

body {
    background: #F9F6EB;
}
.kontainer {
    width: 600px;
    padding: 2em;
    margin: 3em auto;
    background: #fff;
    border-radius: 5px;
    font-family: 'Source Sans Pro', sans-serif;
    border: 1px solid #ddd;
    box-shadow: 0 0 12px #f0e9d1;
    color: #4e4e4e;
}
.kol2 {
    margin: 1em 0;
    display: grid;
    grid-template-columns: 1fr 2fr;
    grid-gap: 1em;
}
.kol3 {
    margin: 1em 0;
    display: grid;
    grid-template-columns: 1fr 2fr 1fr;
    grid-gap: 1em;
}
form {
    margin: 1em 0;
    color: #4e4e4e;
}
label {
    text-align: right;
    align-self: center;
    font-size: 0.9em;
}
input, textarea {
    padding: 0.7em;
    border-radius: 0.3em;
    border: 1px solid #ccc;
    font-weight: bold;
    box-shadow: inset 0 2px 2px rgba(0, 0, 0, 0.1);
}
textarea {
    height: 5em;
    width: 100%;
}
button {
    margin: 1em 0;
    padding: 0.7em;
    border-radius: 0.3em;
    border: none;
    font-weight: bold;
    color: #FFF;
    background-color: #55a5d2;
}
h1, h2, h3 {
    color: #9c813d;
}
h1, h2, h3, p {
    margin: 0.5em 0;
}
h3 {
    margin-top: 2em;
}

table {
    width: 100%;
    border-collapse: collapse;
    margin: 2em 0;
}
th, td {
    padding: 0.5em;
    text-align: left;
}
th {
    background: #305A85;
    color: #FFF;
}
tr:nth-child(even) {
    background: #E6F2F8;
}
tr:nth-child(odd) {
    background: #FFF;
}
table .fa {
    color: #55a5d2;
}
table img {
    width: 50px;
}
form img {
    width: 30px;
}
form label {
    display: flex;
    align-items:center;
}
form span {
    padding: 10px;
}
```
{% endtab %}
{% endtabs %}

Från förra labben kommer vi ihåg att textfilen heter **gastbok.txt**:

```php
<?php
// Namn på textfilen
$filnamn = "gastbok.txt";
?>
```

### file_get_contents()

Vi läser in all text in i en variabel:

```php
// Läs in textfilen
$texten = file_get_contents($filnamn);
```

Och sen skriver ut allt på webbsidan:

```php
// Skriv ut allt
echo $texten;
```

## Inkludera en meny

För att lätt kunna bläddra mellan sidor infogar vi en [bootstrap-navigation](https://getbootstrap.com/docs/5.1/components/navs-tabs/):

```php
<ul class="nav nav-pills">
    <li class="nav-item">
        <a class="nav-link active" aria-current="page" href="#">Active</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="#">Link</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="#">Link</a>
    </li>
    <li class="nav-item">
        <a class="nav-link disabled">Disabled</a>
    </li>
</ul>
```

När vi anpassat menyn ser det ut såhär:

![](<../.gitbook/assets/image (107).png>)

## Uppgifter

* Spara ned texten så det ser ut såhär i textfilen:

![](<../.gitbook/assets/image (106).png>)

* För att kunna spara flera inlägg i gästboken använder man flagga **FILE_APPEND**:

```php
file_put_contents($filnamn, $texten, FILE_APPEND);
```

* Lägg **tid** och **datum** automatiskt i inlägget enligt mha [strftime()](https://devdocs.io/php/function.strftime):

```php
<?php
setlocale(LC_ALL, "sv_SE.utf8");
$dagensDatum = strftime("%A %y %B");
echo "<p>På svenska blir det: $dagensDatum</p>";
echo "<p>Man kan ju börja med stor bokstav: " . ucwords($dagensDatum) . "</p>";
?>
```

![](<../.gitbook/assets/image (108).png>)
